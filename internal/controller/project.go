package controller

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"risk-decision-platform/internal/dao"
	"risk-decision-platform/internal/dao/models"
	"risk-decision-platform/internal/dto"
	"risk-decision-platform/pkg/response"
	"risk-decision-platform/pkg/rule"
	"strings"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func HandleQueryProjectList(c *gin.Context) {
	var body dto.ProjectListRequest
	if err := c.ShouldBind(&body); err != nil {
		c.Error(err)
		return
	}

	var projects []models.Project
	var total int64
	if body.ProjectName != "" {
		if err := dao.Database.Model(&models.Project{}).Where("name like ?", "%"+body.ProjectName+"%").Count(&total).Error; err != nil {
			c.Error(err)
			return
		}
		if err := dao.Database.Where("name like ?", "%"+body.ProjectName+"%").Offset((body.Page - 1) * body.PerPage).Limit(body.PerPage).Find(&projects).Error; err != nil {
			c.Error(err)
			return
		}
	} else {
		if err := dao.Database.Model(&models.Project{}).Count(&total).Error; err != nil {
			c.Error(err)
			return
		}
		if err := dao.Database.Offset((body.Page - 1) * body.PerPage).Limit(body.PerPage).Find(&projects).Error; err != nil {
			c.Error(err)
			return
		}
	}

	c.JSON(http.StatusOK, dto.NewAmisDataListResp(projects, int(total), 0, ""))
}

func HanldeCreateProject(c *gin.Context) {
	var body dto.UpsertProjectRequest
	if err := c.ShouldBindJSON(&body); err != nil {
		c.Error(err)
		return
	}

	record := models.Project{
		Name:        body.Name,
		Description: body.Description,
	}
	if err := dao.Database.Create(&record).Error; err != nil {
		c.Error(err)
		return
	}

	response.Success(c, nil)
}

func HandleUpdateProject(c *gin.Context) {
	var body dto.UpsertProjectRequest
	if err := c.ShouldBindJSON(&body); err != nil {
		c.Error(err)
		return
	}

	var project models.Project
	if err := dao.Database.Where("name = ?", body.Name).First(&project).Error; err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			c.Error(errors.New("project name is not exists"))
			return
		}
		c.Error(err)
		return
	}

	project.Description = body.Description
	if err := dao.Database.Save(&project).Error; err != nil {
		c.Error(err)
		return
	}

	response.Success(c, nil)
}

func HanldeUpdateProjectFeatures(c *gin.Context) {
	var body dto.UpdateProjectFeaturesRequest
	if err := c.ShouldBindJSON(&body); err != nil {
		c.Error(err)
		return
	}

	var project models.Project
	if err := dao.Database.Where("name = ?", body.ProjectName).First(&project).Error; err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			c.Error(errors.New("project name is not exists"))
			return
		}
		c.Error(err)
		return
	}

	record := models.ProjectFeatures{
		ProjectName: body.ProjectName,
		Version:     body.FeatureVersion,
		Features:    body.Features,
	}
	if err := dao.Database.Create(&record).Error; err != nil {
		c.Error(err)
		return
	}

	response.Success(c, nil)
}

func HanldeUpdateProjectModels(c *gin.Context) {
	var body dto.UpdateProjectModelsRequest
	if err := c.ShouldBindJSON(&body); err != nil {
		c.Error(err)
		return
	}

	var project models.Project
	if err := dao.Database.Where("name = ?", body.ProjectName).First(&project).Error; err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			c.Error(errors.New("project name is not exists"))
			return
		}
	}

	record := models.ProjectModels{
		ProjectName: body.ProjectName,
		Version:     body.ModelVersion,
		Models:      body.Models,
	}
	if err := dao.Database.Create(&record).Error; err != nil {
		c.Error(err)
		return
	}

	response.Success(c, nil)
}

func HandleQueryProjectFeatures(c *gin.Context) {
	projectName := c.Query("projectName")
	if projectName == "" {
		c.Error(errors.New("projectName is required"))
		return
	}

	data := make(map[string]interface{})

	// feature options
	var features []models.RiskFeature
	if err := dao.Database.Order("created_at desc").Find(&features).Error; err != nil {
		c.Error(err)
		return
	}
	var featureOptions = make([]dto.Option, 0)
	for _, feature := range features {
		featureOptions = append(featureOptions, dto.Option{
			Label: feature.Name,
			Value: feature.Name,
			Tag:   feature.Description,
		})
	}
	data["featureOptions"] = featureOptions

	// project features
	var project models.Project
	if err := dao.Database.Where("name = ?", projectName).First(&project).Error; err != nil {
		c.Error(err)
		return
	}

	var record models.ProjectFeatures
	if err := dao.Database.Where("project_name = ?", projectName).Order("id desc").First(&record).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			data["featureVersion"] = "1.0.0.0"
			data["projectFeatures"] = ""
			c.JSON(http.StatusOK, dto.NewAmisSuccessResp(data))
			return
		}
		c.Error(err)
		return
	}

	data["featureVersion"] = record.Version
	data["projectFeatures"] = record.Features

	var projectFeatures = make([]dto.Option, 0)
	for _, feature := range strings.Split(record.Features, ",") {
		projectFeatures = append(projectFeatures, dto.Option{Label: feature, Value: feature, Tag: feature})
	}

	data["items"] = projectFeatures

	c.JSON(http.StatusOK, dto.NewAmisSuccessResp(data))
}

func HandleQueryProjectModels(c *gin.Context) {
	projectName := c.Query("projectName")
	if projectName == "" {
		c.Error(errors.New("projectName is required"))
		return
	}

	data := make(map[string]interface{})

	// model options
	var riskModels []models.RiskModel
	if err := dao.Database.Order("created_at desc").Find(&riskModels).Error; err != nil {
		c.Error(err)
		return
	}
	var modelOptions = make([]dto.Option, 0)
	for _, model := range riskModels {
		modelOptions = append(modelOptions, dto.Option{
			Label: model.Name,
			Value: model.Name,
			Tag:   model.Description,
		})
	}
	data["modelOptions"] = modelOptions

	// project models
	var project models.Project
	if err := dao.Database.Where("name = ?", projectName).First(&project).Error; err != nil {
		c.Error(err)
		return
	}

	var record models.ProjectModels
	if err := dao.Database.Where("project_name = ?", projectName).Order("id desc").First(&record).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			data["modelVersion"] = "1.0.0.0"
			data["projectModels"] = ""
			c.JSON(http.StatusOK, dto.NewAmisSuccessResp(data))
			return
		}
		c.Error(err)
		return
	}

	data["modelVersion"] = record.Version
	data["projectModels"] = record.Models

	var projectModels = make([]dto.Option, 0)
	for _, model := range strings.Split(record.Models, ",") {
		projectModels = append(projectModels, dto.Option{Label: model, Value: model, Tag: model})
	}

	data["items"] = projectModels

	c.JSON(http.StatusOK, dto.NewAmisSuccessResp(data))
}

func HandleQueryProjectFeaturesAndModels(c *gin.Context) {
	projectName := c.Query("projectName")
	if projectName == "" {
		c.Error(errors.New("projectName is required"))
		return
	}

	var result = make(map[string][]dto.FieldOption)

	var project models.Project
	if err := dao.Database.Where("name = ?", projectName).First(&project).Error; err != nil {
		c.Error(err)
		return
	}

	// query features
	var features = make([]string, 0)
	var projectFeature models.ProjectFeatures
	if err := dao.Database.Where("project_name = ?", projectName).Order("id desc").First(&projectFeature).Error; err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			c.Error(err)
			return
		}
	} else {
		features = strings.Split(projectFeature.Features, ",")
	}

	var riskFeatures []models.RiskFeature
	if len(features) > 0 {
		if err := dao.Database.Where("name in (?)", features).Find(&riskFeatures).Error; err != nil {
			c.Error(err)
			return
		}
	}

	// query models
	var modelNames = make([]string, 0)
	var projectModel models.ProjectModels
	if err := dao.Database.Where("project_name = ?", projectName).Order("id desc").First(&projectModel).Error; err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			c.Error(err)
			return
		}
	} else {
		modelNames = strings.Split(projectModel.Models, ",")
	}

	var riskModels []models.RiskModel
	if len(modelNames) > 0 {
		if err := dao.Database.Where("name in (?)", modelNames).Find(&riskModels).Error; err != nil {
			c.Error(err)
			return
		}
	}

	variables := make([]dto.FieldOption, 0)
	for _, feature := range riskFeatures {
		fieldOptionValue, operators, err := rule.GetConditionOperators(feature.ValueType)
		if err != nil {
			c.Error(err)
			return
		}
		variables = append(variables, dto.FieldOption{
			Label:     feature.Name,
			Name:      feature.Name,
			Type:      "custom",
			Value:     fieldOptionValue,
			Operators: operators,
		})
	}
	for _, model := range riskModels {
		fieldOptionValue, operators, err := rule.GetConditionOperators("float")
		if err != nil {
			c.Error(err)
			return
		}
		modelOutput := strings.Split(strings.Trim(model.Output, "[]"), ",")
		var children = make([]dto.FieldOption, 0)
		for _, mopt := range modelOutput {
			children = append(children, dto.FieldOption{
				Label:     mopt,
				Name:      mopt,
				Type:      "custom",
				Value:     fieldOptionValue,
				Operators: operators,
			})
		}
		variables = append(variables, dto.FieldOption{
			Label:    model.Name,
			Children: children,
		})
	}

	result["fields"] = variables

	c.JSON(http.StatusOK, dto.NewAmisSuccessResp(result))
}

func HandleValidateProjectRuleSet(c *gin.Context) {
	var body dto.UpdateProjectRuleSetRequest
	if err := c.ShouldBindJSON(&body); err != nil {
		c.Error(err)
		return
	}

	// validate version
	var cnt int64
	if err := dao.Database.Model(&models.ProjectRuleset{}).Where("project_name = ? AND version = ?", body.ProjectName, body.RulesetVersion).Count(&cnt).Error; err != nil {
		c.Error(err)
		return
	}
	if cnt > 0 {
		c.Error(errors.New("project ruleset version is already exists"))
		return
	}

	// validate default output
	defaultOutputVars := make(map[string]string)
	for _, optVar := range body.OutputDefaultVars {
		if _, err := rule.ValidateRuleSetOutput(optVar); err != nil {
			c.Error(err)
			return
		}
		defaultOutputVars[optVar.VarName] = optVar.VarType
	}

	// validate ruleset output
	for _, ruleset := range body.RuleSet {
		for _, optVar := range ruleset.RuleOutput {
			varType, ok := defaultOutputVars[optVar.VarName]
			if !ok {
				c.Error(fmt.Errorf("the var name is not exists in default output vars: %s", optVar.VarName))
				return
			}
			optVar.VarType = varType
			if _, err := rule.ValidateRuleSetOutput(optVar); err != nil {
				c.Error(fmt.Errorf("rule name: %s, %s", ruleset.RuleName, err.Error()))
				return
			}
		}
	}

	response.Success(c, nil)
}

func HandleUpdateProjectRuleSet(c *gin.Context) {
	var body dto.UpdateProjectRuleSetRequest
	if err := c.ShouldBindJSON(&body); err != nil {
		c.Error(err)
		return
	}

	bytes, err := json.Marshal(body)
	if err != nil {
		c.Error(err)
		return
	}

	var project models.Project
	if err := dao.Database.Where("name = ?", body.ProjectName).First(&project).Error; err != nil {
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			c.Error(errors.New("project name is not exists"))
			return
		}
		c.Error(err)
		return
	}

	record := models.ProjectRuleset{
		ProjectName: body.ProjectName,
		Version:     body.RulesetVersion,
		Ruleset:     string(bytes),
	}

	if err := dao.Database.Save(&record).Error; err != nil {
		if errors.Is(err, gorm.ErrDuplicatedKey) {
			c.Error(errors.New("project ruleset version already exists"))
			return
		}
		c.Error(err)
		return
	}

	response.Success(c, nil)
}

func HandleQueryProjectRuleSet(c *gin.Context) {
	projectName := c.Query("projectName")
	if projectName == "" {
		c.Error(errors.New("projectName is required"))
		return
	}

	data := make(map[string]interface{})

	var ruleset models.ProjectRuleset
	if err := dao.Database.Where("project_name = ?", projectName).Order("id desc").First(&ruleset).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			response.Success(c, nil)
			return
		}
		c.Error(err)
		return
	}

	if err := json.Unmarshal([]byte(ruleset.Ruleset), &data); err != nil {
		c.Error(err)
		return
	}

	c.JSON(http.StatusOK, dto.NewAmisSuccessResp(data))
}

func HandleQueryProjectRuleSetCurrentVersion(c *gin.Context) {
	projectName := c.Query("projectName")
	if projectName == "" {
		c.Error(errors.New("projectName is required"))
		return
	}

	data := make(map[string]interface{})
	data["rulesetVersion"] = "1.0.0"

	var ruleset models.ProjectRuleset
	if err := dao.Database.Where("project_name = ?", projectName).Order("id desc").First(&ruleset).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			c.JSON(http.StatusOK, dto.NewAmisSuccessResp(data))
			return
		}
		c.Error(err)
		return
	}

	data["rulesetVersion"] = ruleset.Version
	c.JSON(http.StatusOK, dto.NewAmisSuccessResp(data))
}

func HandleQueryProjectRuleSetVersionAll(c *gin.Context) {
	projectName := c.Query("projectName")
	if projectName == "" {
		c.Error(errors.New("projectName is required"))
		return
	}

	var options = make([]dto.Option, 0)
	var records []models.ProjectRuleset
	if err := dao.Database.Where("project_name = ?", projectName).Order("id desc").Find(&records).Error; err != nil {
		c.Error(err)
		return
	}

	for _, record := range records {
		options = append(options, dto.Option{
			Label: record.Version,
			Value: record.Version,
		})
	}

	c.JSON(http.StatusOK, dto.NewAmisDataOptionResp(options, 0, ""))
}

func HandleProjectRuleSetExecute(c *gin.Context) {
	projectName := c.Query("projectName")
	if projectName == "" {
		c.Error(errors.New("projectName is required"))
		return
	}

	var body dto.ProjectSingleExecuteRequest
	if err := c.ShouldBindJSON(&body); err != nil {
		c.Error(err)
		return
	}

	var record models.ProjectRuleset
	if err := dao.Database.Where("project_name = ? AND version = ?", projectName, body.RulesetVersion).First(&record).Error; err != nil {
		c.Error(err)
		return
	}

	var caseData map[string]interface{}
	if err := json.Unmarshal([]byte(body.CaseData), &caseData); err != nil {
		c.Error(err)
		return
	}

	var ruleSetReq dto.UpdateProjectRuleSetRequest
	if err := json.Unmarshal([]byte(record.Ruleset), &ruleSetReq); err != nil {
		c.Error(err)
		return
	}

	output, executeLog, err := rule.ExecuteProjectRuleSet(ruleSetReq.OutputDefaultVars, ruleSetReq.RuleSet, caseData)
	if err != nil {
		c.Error(err)
		return
	}

	data := dto.ProjectSingleExecuteResponse{
		OutputData: output,
		ExecuteLog: executeLog,
	}

	c.JSON(http.StatusOK, dto.NewAmisSuccessResp(data))
}
