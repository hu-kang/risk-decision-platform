package controller

import (
	"net/http"
	"risk-decision-platform/internal/dao"
	"risk-decision-platform/internal/dao/models"
	"risk-decision-platform/internal/dto"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// get model list
func HandleQueryModelList(c *gin.Context) {
	var body dto.ModelListRequest
	if err := c.ShouldBind(&body); err != nil {
		c.Error(err)
		return
	}

	var tx *gorm.DB
	var riskModels []models.RiskModel
	if body.Name != "" {
		tx = dao.Database.Where("name = ?", body.Name).Find(&riskModels)
	} else if body.Feature != "" {
		tx = dao.Database.Where("features LIKE ?", "%"+body.Feature+"%").Find(&riskModels)
	} else if body.Output != "" {
		tx = dao.Database.Where("output LIKE ?", "%"+body.Output+"%").Find(&riskModels)
	} else {
		tx = dao.Database.Find(&riskModels)
	}

	totla := len(riskModels)

	var data []models.RiskModel
	if body.Page > 0 && body.PerPage > 0 {
		if err := tx.Order("created_at desc").Offset((body.Page - 1) * body.PerPage).Limit(body.PerPage).Find(&data).Error; err != nil {
			c.Error(err)
			return
		}
	} else {
		if err := tx.Order("created_at desc").Limit(10).Find(&data).Error; err != nil {
			c.Error(err)
		}
	}

	c.JSON(http.StatusOK, dto.NewAmisDataListResp(data, totla, 0, ""))
}
