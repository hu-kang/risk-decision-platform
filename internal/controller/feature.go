package controller

import (
	"fmt"
	"net/http"
	"risk-decision-platform/internal/config"
	"risk-decision-platform/internal/dao"
	"risk-decision-platform/internal/dto"
	"risk-decision-platform/pkg/util"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// get feature option list
func HandleQueryFeatureOptions(c *gin.Context) {
	collection := dao.MongoClient.Database("ppdai_intl_risk").Collection("risk_variables")

	filter := bson.D{}
	filter = append(filter, bson.E{Key: "operator_type", Value: "var_logic"})

	cur, err := collection.Find(c, filter)
	if err != nil {
		c.Error(err)
		return
	}

	results := make([]map[string]interface{}, 0)
	if err := cur.All(c, &results); err != nil {
		c.Error(err)
		return
	}

	options := make([]dto.Option, 0)
	for _, result := range results {
		options = append(options, dto.Option{
			Label: result["operator_name"].(string),
			Value: result["operator_name"].(string),
		})
	}

	c.JSON(http.StatusOK, dto.NewAmisDataOptionResp(options, 0, ""))
}

// get feature list
func HandleQueryFeatureList(c *gin.Context) {
	var body dto.FeatureListRequest
	if err := c.ShouldBind(&body); err != nil {
		c.Error(err)
		return
	}

	collection := dao.MongoClient.Database("ppdai_intl_risk").Collection("risk_variables")

	filter := bson.D{}
	filter = append(filter, bson.E{Key: "operator_type", Value: "var_logic"})
	if body.Name != "" {
		filter = append(filter, bson.E{
			Key:   "var_name",
			Value: bson.M{"$regex": body.Name, "$options": "i"},
		})
	}
	if body.Dependency != "" {
		filter = append(filter, bson.E{
			Key: "dependency",
			Value: bson.M{
				"$elemMatch": bson.M{"$regex": body.Dependency, "$options": "i"},
			},
		})
	}

	options := options.Find()

	totalCount, err := collection.CountDocuments(c, filter)
	if err != nil {
		c.Error(err)
		return
	}

	if body.Page > 0 && body.PerPage > 0 {
		options = options.SetSkip(int64((body.Page - 1) * body.PerPage)).SetLimit(int64(body.PerPage))
	} else {
		options = options.SetLimit(10)
	}

	options = options.SetSort(bson.D{{Key: "create_time", Value: -1}})

	cur, err := collection.Find(c, filter, options)
	if err != nil {
		c.Error(err)
		return
	}

	var results = make([]bson.M, 0)
	if err := cur.All(c, &results); err != nil {
		c.Error(err)
		return
	}

	c.JSON(http.StatusOK, dto.NewAmisDataListResp(results, int(totalCount), 0, ""))
}

// add feature
func HandleCreateFeature(c *gin.Context) {
	var body dto.FeatureRequest
	if err := c.ShouldBind(&body); err != nil {
		c.Error(err)
		return
	}

	collection := dao.MongoClient.Database("ppdai_intl_risk").Collection("risk_variables")

	defaultValue, err := util.GetDefaultValue(body.ValueType, body.DefaultValue)
	if err != nil {
		c.Error(err)
		return
	}

	if _, err := collection.InsertOne(c, bson.D{
		{Key: "operator_name", Value: fmt.Sprintf("%s.%s", body.Name, "var_logic")},
		{Key: "operator_type", Value: "var_logic"},
		{Key: "var_name", Value: body.Name},
		{Key: "description", Value: body.Description},
		{Key: "sql_logic", Value: body.Loigc},
		{Key: "value_type", Value: body.ValueType},
		{Key: "default_value", Value: defaultValue},
		{Key: "dependency", Value: body.Dependency},
		{Key: "create_time", Value: time.Now().Unix()},
		{Key: "create_time_str", Value: time.Now().Format(config.TimeFormat)},
		{Key: "update_time", Value: time.Now().Unix()},
		{Key: "update_time_str", Value: time.Now().Format(config.TimeFormat)},
		{Key: "is_active", Value: true},
	}); err != nil {
		c.Error(err)
		return
	}

	c.JSON(http.StatusOK, dto.NewAmisSuccessResp(nil))
}

// update feature
func HandleUpdateFeature(c *gin.Context) {
	var body dto.FeatureRequest
	if err := c.ShouldBind(&body); err != nil {
		c.Error(err)
		return
	}

	collection := dao.MongoClient.Database("ppdai_intl_risk").Collection("risk_variables")

	filter := bson.D{
		{Key: "var_name", Value: body.Name},
	}

	defaultValue, err := util.GetDefaultValue(body.ValueType, body.DefaultValue)
	if err != nil {
		c.Error(err)
		return
	}

	update := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "description", Value: body.Description},
			{Key: "sql_logic", Value: body.Loigc},
			{Key: "value_type", Value: body.ValueType},
			{Key: "default_value", Value: defaultValue},
			{Key: "dependency", Value: body.Dependency},
			{Key: "update_time", Value: time.Now().Unix()},
			{Key: "update_time_str", Value: time.Now().Format(config.TimeFormat)},
		}},
	}

	if _, err := collection.UpdateOne(c, filter, update); err != nil {
		c.Error(err)
		return
	}

	c.JSON(http.StatusOK, dto.NewAmisSuccessResp(nil))
}

// get data source option list
func HandleQueryDataSourceOptions(c *gin.Context) {
	collection := dao.MongoClient.Database("ppdai_intl_risk").Collection("risk_variables")

	filter := bson.D{}
	filter = append(filter, bson.E{Key: "operator_type", Value: "data_source"})

	cur, err := collection.Find(c, filter)
	if err != nil {
		c.Error(err)
		return
	}

	results := make([]map[string]interface{}, 0)
	if err := cur.All(c, &results); err != nil {
		c.Error(err)
		return
	}

	options := make([]dto.Option, 0)
	options = append(options, dto.Option{
		Label: "request_body",
		Value: "request_body",
	})
	for _, result := range results {
		options = append(options, dto.Option{
			Label: result["operator_name"].(string),
			Value: result["operator_name"].(string),
		})
	}

	c.JSON(http.StatusOK, dto.NewAmisDataOptionResp(options, 0, ""))
}

// add data source
func HandleCreateDataSource(c *gin.Context) {
	var body dto.DataSourceRequest
	if err := c.ShouldBind(&body); err != nil {
		c.Error(err)
		return
	}

	collection := dao.MongoClient.Database("ppdai_intl_risk").Collection("risk_variables")

	if _, err := collection.InsertOne(c, bson.D{
		{Key: "operator_name", Value: fmt.Sprintf("%s.%s.by_%s", body.DbName, body.TableName, body.QueryKey)},
		{Key: "operator_type", Value: "data_source"},
		{Key: "db_name", Value: body.DbName},
		{Key: "table_name", Value: body.TableName},
		{Key: "sql_logic", Value: body.Loigc},
		{Key: "dependency", Value: body.Dependency},
		{Key: "create_time", Value: time.Now().Unix()},
		{Key: "create_time_str", Value: time.Now().Format(config.TimeFormat)},
		{Key: "update_time", Value: time.Now().Unix()},
		{Key: "update_time_str", Value: time.Now().Format(config.TimeFormat)},
		{Key: "is_active", Value: true},
	}); err != nil {
		c.Error(err)
		return
	}

	c.JSON(http.StatusOK, dto.NewAmisSuccessResp(nil))
}

// update data source
func HandleUpdateDataSource(c *gin.Context) {
	var body dto.DataSourceRequest
	if err := c.ShouldBind(&body); err != nil {
		c.Error(err)
		return
	}

	collection := dao.MongoClient.Database("ppdai_intl_risk").Collection("risk_variables")

	filter := bson.D{
		{Key: "operator_name", Value: fmt.Sprintf("%s.%s.by_%s", body.DbName, body.TableName, body.QueryKey)},
	}

	update := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "sql_logic", Value: body.Loigc},
			{Key: "query_key", Value: body.QueryKey},
			{Key: "dependency", Value: body.Dependency},
			{Key: "update_time", Value: time.Now().Unix()},
			{Key: "update_time_str", Value: time.Now().Format(config.TimeFormat)},
		}},
	}

	if _, err := collection.UpdateOne(c, filter, update); err != nil {
		c.Error(err)
		return
	}

	c.JSON(http.StatusOK, dto.NewAmisSuccessResp(nil))
}
