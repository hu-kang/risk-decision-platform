package server

import (
	"net/http"
	"risk-decision-platform/internal/controller"

	"github.com/gin-gonic/gin"
)

func RegisterRouter(engine *gin.Engine) {
	// 404
	engine.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{
			"code":    404,
			"message": "Not Found",
		})
	})
	// index
	engine.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{})
	})
	// hs
	engine.GET("/hs", func(c *gin.Context) {
		c.String(http.StatusOK, "OK")
	})
	// feature
	engine.GET("/feature/options", controller.HandleQueryFeatureOptions)
	engine.GET("/feature/list", controller.HandleQueryFeatureList)
	engine.POST("/feature/create", controller.HandleCreateFeature)
	engine.POST("/feature/update", controller.HandleUpdateFeature)
	engine.GET("/datasource/options", controller.HandleQueryDataSourceOptions)
	engine.POST("/datasource/create", controller.HandleCreateDataSource)
	engine.POST("/datasource/update", controller.HandleUpdateDataSource)
	// model
	engine.GET("/models/list", controller.HandleQueryModelList)
	// project
	engine.GET("/project/list", controller.HandleQueryProjectList)
	engine.POST("/project/create", controller.HanldeCreateProject)
	engine.POST("/project/update", controller.HandleUpdateProject)
	engine.GET("/project/features", controller.HandleQueryProjectFeatures)
	engine.GET("/project/models", controller.HandleQueryProjectModels)
	engine.POST("/project/features/update", controller.HanldeUpdateProjectFeatures)
	engine.POST("/project/models/update", controller.HanldeUpdateProjectModels)
	engine.GET("/project/featuresAndModels", controller.HandleQueryProjectFeaturesAndModels)
	engine.GET("/project/ruleset", controller.HandleQueryProjectRuleSet)
	engine.POST("/project/ruleset/validate", controller.HandleValidateProjectRuleSet)
	engine.POST("/project/ruleset/update", controller.HandleUpdateProjectRuleSet)
	engine.GET("/project/ruleset/version", controller.HandleQueryProjectRuleSetCurrentVersion)
	engine.GET("/project/ruleset/version/all", controller.HandleQueryProjectRuleSetVersionAll)
	engine.POST("/project/ruleset/execute", controller.HandleProjectRuleSetExecute)
}
