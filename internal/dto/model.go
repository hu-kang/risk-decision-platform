package dto

type ModelListRequest struct {
	CommonListRequest
	Name    string `form:"name"`
	Feature string `form:"feature"`
	Output  string `form:"output"`
}
