package dto

type FeatureListRequest struct {
	CommonListRequest
	Name       string `form:"name"`
	Dependency string `form:"dependency"`
}

type FeatureRequest struct {
	Name         string      `json:"var_name" binding:"required"`
	Description  string      `json:"description" binding:"required"`
	Loigc        string      `json:"sql_logic" binding:"required"`
	ValueType    string      `json:"value_type" binding:"required"`
	DefaultValue interface{} `json:"default_value"`
	Dependency   []string    `json:"dependency" binding:"required"`
}

type DataSourceListRequest struct {
	CommonListRequest
	DbName    string `form:"db_name"`
	TableName string `form:"table_name"`
}

type DataSourceRequest struct {
	DbName     string   `json:"db_name" binding:"required"`
	TableName  string   `json:"table_name" binding:"required"`
	Loigc      string   `json:"sql_logic" binding:"required"`
	QueryKey   string   `json:"query_key" binding:"required"`
	Dependency []string `json:"dependency" binding:"required"`
}
