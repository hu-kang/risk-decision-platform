package dto

type ProjectListRequest struct {
	CommonListRequest
	ProjectName string `form:"projectName"`
}

type UpsertProjectRequest struct {
	Name        string `json:"name" binding:"required"`
	Description string `json:"description" binding:"required"`
}

type UpdateProjectFeaturesRequest struct {
	ProjectName    string `json:"projectName" binding:"required"`
	FeatureVersion string `json:"featureVersion" binding:"required"`
	Features       string `json:"features" binding:"required"`
}

type UpdateProjectModelsRequest struct {
	ProjectName  string `json:"projectName" binding:"required"`
	ModelVersion string `json:"modelVersion" binding:"required"`
	Models       string `json:"models" binding:"required"`
}

type (
	OutputVar struct {
		VarName  string `json:"varName"`
		VarDesc  string `json:"varDesc"`
		VarType  string `json:"varType"`
		VarValue string `json:"varValue"`
	}
	ConditionLeft struct {
		Field string `json:"field"`
		Type  string `json:"type"`
	}
	Condition struct {
		ID    string        `json:"id"`
		Left  ConditionLeft `json:"left"`
		Op    string        `json:"op"`
		Right interface{}   `json:"right"`
	}
	RuleConditions struct {
		ID          string `json:"id"`
		Conjunction string `json:"conjunction"`
		Children    []any  `json:"children" binding:"required,min=1"`
	}
	RuleSet struct {
		RuleName       string         `json:"ruleName"`
		RuleOutput     []OutputVar    `json:"ruleOutput"`
		RuleConditions RuleConditions `json:"ruleConditions"`
	}
	UpdateProjectRuleSetRequest struct {
		ProjectName       string      `json:"projectName" binding:"required"`
		RulesetVersion    string      `json:"rulesetVersion" binding:"required"`
		OutputDefaultVars []OutputVar `json:"outputDefaultVars" binding:"required,min=1"`
		RuleSet           []RuleSet   `json:"ruleSet" binding:"required"`
	}
)

type (
	ProjectSingleExecuteRequest struct {
		RulesetVersion string `json:"rulesetVersion" binding:"required"`
		CaseData       string `json:"caseData" binding:"required"`
	}
	ProjectSingleExecuteResponse struct {
		OutputData interface{} `json:"outputData"`
		ExecuteLog string      `json:"executeLog"`
	}
)
