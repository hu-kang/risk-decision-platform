package dto

type CommonListRequest struct {
	Page    int `form:"page"`
	PerPage int `form:"perPage"`
}

type AmisDataList struct {
	Items interface{} `json:"items"`
	Total int         `json:"total"`
}

type FieldOption struct {
	Label     string                 `json:"label"`
	Name      string                 `json:"name"`
	Type      string                 `json:"type"`
	Value     map[string]interface{} `json:"value"`
	Operators interface{}            `json:"operators"`
	Children  []FieldOption          `json:"children"`
}

type Option struct {
	Label string      `json:"label"`
	Value interface{} `json:"value"`
	Tag   string      `json:"tag"`
}

type AmisDataOption struct {
	Options []Option `json:"options"`
}

type AmisDataResp struct {
	Status int         `json:"status"`
	Msg    string      `json:"msg"`
	Data   interface{} `json:"data"`
}

func NewAmisSuccessResp(data interface{}) AmisDataResp {
	return AmisDataResp{
		Status: 0,
		Msg:    "success",
		Data:   data,
	}
}

func NewAmisfailResp(msg string) AmisDataResp {
	return AmisDataResp{
		Status: 1,
		Msg:    msg,
		Data:   nil,
	}
}

func NewAmisDataListResp(items interface{}, total int, status int, msg string) AmisDataResp {
	return AmisDataResp{
		Status: status,
		Msg:    msg,
		Data: AmisDataList{
			Items: items,
			Total: total,
		},
	}
}

func NewAmisDataOptionResp(options []Option, status int, msg string) AmisDataResp {
	return AmisDataResp{
		Status: status,
		Msg:    msg,
		Data: AmisDataOption{
			Options: options,
		},
	}
}
