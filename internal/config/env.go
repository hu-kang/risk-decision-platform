package config

import (
	"log"

	"github.com/kelseyhightower/envconfig"
)

const (
	ENV_DEV = "dev"
)

type ENVConfig struct {
	AppId   string `envconfig:"APP_ID"`
	AppName string `envconfig:"APP_NAME"`
	ENV     string `envconfig:"ENV" default:"dev"`
}

var ENVConf ENVConfig

func init() {
	err := envconfig.Process("", &ENVConf)
	if err != nil {
		panic(err)
	}
	log.Printf("env config: %+v\n", ENVConf)
}

func IsInDev() bool {
	return ENVConf.ENV == ENV_DEV
}
