package config

import (
	_ "embed"
	"log"
	"risk-decision-platform/pkg/database"

	"gopkg.in/yaml.v3"
)

type ServerSetting struct {
	RiskPgConfig    database.PGConfig    `yaml:"risk_pg_config"`
	RiskMongoConfig database.MongoConfig `yaml:"risk_mongo_config"`
}

//go:embed application-local.yaml
var localConfigBytes []byte

func BuildSetting(setting *ServerSetting) {
	if IsInDev() {
		err := yaml.Unmarshal(localConfigBytes, &setting)
		if err != nil {
			panic(err)
		}
	} else {
		setting = &ServerSetting{}
	}
}

var Setting ServerSetting

func init() {
	log.Println("setting init")
	BuildSetting(&Setting)
	log.Printf("setting init success, setting: %+v", Setting)
}
