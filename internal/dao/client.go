package dao

import (
	"risk-decision-platform/internal/config"
	"risk-decision-platform/internal/dao/models"
	"risk-decision-platform/pkg/database"

	"go.mongodb.org/mongo-driver/mongo"
	"gorm.io/gorm"
)

var Database *gorm.DB
var MongoClient *mongo.Client

func init() {
	Database = database.BuildPGClient(config.Setting.RiskPgConfig)
	MongoClient = database.BuildMongoClient(config.Setting.RiskMongoConfig)

	// Migrate the schema
	Database.AutoMigrate(
		&models.RiskModel{},
		&models.Project{},
		&models.ProjectFeatures{},
		&models.ProjectModels{},
		&models.ProjectRuleset{},
	)
}
