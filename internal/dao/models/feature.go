package models

import "gorm.io/gorm"

type RiskFeature struct {
	gorm.Model
	Name        string `json:"name" gorm:"unique"`
	Description string `json:"description"`
	Logic       string `json:"logic"`
	ValueType   string `json:"value_type"`
}
