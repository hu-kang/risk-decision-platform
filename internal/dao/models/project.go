package models

import "gorm.io/gorm"

type Project struct {
	gorm.Model
	Name        string `json:"name" gorm:"unique"`
	Description string `json:"description"`
}

type ProjectFeatures struct {
	gorm.Model
	ProjectName string `json:"project_name"`
	Version     string `json:"version"`
	Features    string `json:"features"`
}

type ProjectModels struct {
	gorm.Model
	ProjectName string `json:"project_name"`
	Version     string `json:"version"`
	Models      string `json:"models"`
}

type ProjectRuleset struct {
	gorm.Model
	ProjectName string `json:"project_name"`
	Version     string `json:"version"`
	Ruleset     string `json:"ruleset"`
}
