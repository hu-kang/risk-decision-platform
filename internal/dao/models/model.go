package models

import "gorm.io/gorm"

type RiskModel struct {
	gorm.Model
	Name        string `json:"name" gorm:"unique"`
	Description string `json:"description"`
	Features    string `json:"features"`
	Output      string `json:"output"`
}
