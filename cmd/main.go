package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"risk-decision-platform/internal/server"
	"risk-decision-platform/pkg/middleware"
	"time"

	"github.com/gin-gonic/gin"
)

const (
	serverTimeout = 5 * time.Second
	serverAddr    = ":8000"
)

func main() {
	engine := gin.Default()

	engine.Static("/static", "../static")
	engine.LoadHTMLGlob("../static/templates/*")

	// use middleware
	engine.Use(gin.Logger(), middleware.CustomeRecover, middleware.HandleError)

	// register router
	server.RegisterRouter(engine)

	srv := &http.Server{
		Addr:    serverAddr,
		Handler: engine,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), serverTimeout)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	log.Println("Server exiting")
}
