package rule

import (
	"strings"

	"github.com/Knetic/govaluate"
)

var RuleEngineFuncs map[string]govaluate.ExpressionFunction

func init() {
	RuleEngineFuncs = make(map[string]govaluate.ExpressionFunction)
	RuleEngineFuncs["is_null"] = IsNull
	RuleEngineFuncs["is_not_null"] = IsNotNull
	RuleEngineFuncs["starts_with"] = StartsWith
	RuleEngineFuncs["ends_with"] = EndsWith
}

func IsNull(argus ...interface{}) (interface{}, error) {
	if len(argus) == 0 {
		return true, nil
	}
	return argus[0] == nil, nil
}

func IsNotNull(argus ...interface{}) (interface{}, error) {
	if len(argus) == 0 {
		return false, nil
	}
	return argus[0] != nil, nil
}

func StartsWith(argus ...interface{}) (interface{}, error) {
	val := argus[0].(string)
	prefix := argus[1].(string)
	return strings.HasPrefix(val, prefix), nil
}

func EndsWith(argus ...interface{}) (interface{}, error) {
	val := argus[0].(string)
	suffix := argus[1].(string)
	return strings.HasSuffix(val, suffix), nil
}
