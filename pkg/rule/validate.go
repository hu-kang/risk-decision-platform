package rule

import (
	"fmt"
	"risk-decision-platform/internal/dto"

	"github.com/spf13/cast"
)

func ValidateRuleSetOutput(optVar dto.OutputVar) (val interface{}, err error) {
	switch optVar.VarType {
	case "string":
		val, err = cast.ToStringE(optVar.VarValue)
	case "int":
		val, err = cast.ToIntE(optVar.VarValue)
	case "float":
		val, err = cast.ToFloat64E(optVar.VarValue)
	}

	if err != nil {
		err = fmt.Errorf("the var type does not match the var value: %s", optVar.VarName)
		return
	}

	return
}
