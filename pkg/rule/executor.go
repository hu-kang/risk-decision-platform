package rule

import (
	"encoding/json"
	"fmt"
	"risk-decision-platform/internal/dto"
	"strings"

	"github.com/Knetic/govaluate"
	mapset "github.com/deckarep/golang-set/v2"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
)

var LogicalOperators = map[string]interface{}{
	"and": "&&",
	"or":  "||",
}

var ConditionOperators = map[string]interface{}{
	"is_empty":         "== nil",
	"is_not_empty":     "!= nil",
	"equal":            "==",
	"not_equal":        "!=",
	"less":             "<",
	"less_or_equal":    "<=",
	"greater":          ">",
	"greater_or_equal": ">=",
	"like":             "=~",
	"not_like":         "!~",
	"starts_with":      "starts_with",
	"ends_with":        "ends_with",
	"in":               "in",
	"not_in":           "not_in",
}

func GetConditionOperators(fieldType string) (map[string]interface{}, []interface{}, error) {
	operatorKeys := mapset.NewSetFromMapKeys(ConditionOperators)

	fieldOptionValue := make(map[string]interface{})

	switch fieldType {
	case "string":
		fieldOptionValue["type"] = "input-text"
		operatorKeys.RemoveAll("less", "less_or_equal", "greater", "greater_or_equal")
	case "int":
		fieldOptionValue["type"] = "input-number"
		operatorKeys.RemoveAll("like", "not_like", "starts_with", "ends_with")
	case "float":
		fieldOptionValue["type"] = "input-number"
		fieldOptionValue["precision"] = 4
		operatorKeys.RemoveAll("like", "not_like", "starts_with", "ends_with")
	default:
		return nil, nil, fmt.Errorf("unsupported field type: %s", fieldType)
	}

	operators := operatorKeys.ToSlice()
	res := make([]interface{}, len(operators))

	var buildOp = func(label, value, fieldType string) gin.H {
		return gin.H{
			"label": label,
			"value": value,
			"values": []gin.H{
				{
					"type":      "input-array",
					"name":      "members",
					"inline":    true,
					"minLength": 1,
					"items":     gin.H{"type": fmt.Sprintf("input-%s", fieldType)}}},
		}
	}

	for index, op := range operators {
		if op == "in" {
			res[index] = buildOp("在集合内", "in", fieldType)
		} else if op == "not_in" {
			res[index] = buildOp("不在集合内", "not_in", fieldType)
		} else {
			res[index] = op
		}
	}

	return fieldOptionValue, res, nil
}

func convertToCondition(src interface{}) (interface{}, error) {
	bytes, err := json.Marshal(src)
	if err != nil {
		return nil, err
	}

	switch m := src.(type) {
	case map[string]interface{}:
		if _, ok := m["children"]; !ok {
			var condition dto.Condition
			err = json.Unmarshal(bytes, &condition)
			return condition, err
		} else {
			var cdv dto.RuleConditions
			err = json.Unmarshal(bytes, &cdv)
			return cdv, err
		}
	default:
		return nil, fmt.Errorf("unknown type")
	}
}

func ParseCondition(conditions dto.RuleConditions) (string, error) {
	if len(conditions.Children) == 0 {
		return "", fmt.Errorf("empty condition group")
	}

	cdExprs := make([]string, 0)

	for _, child := range conditions.Children {
		condition, err := convertToCondition(child)
		if err != nil {
			return "", err
		}
		switch cdv := condition.(type) {
		case dto.Condition:
			switch cdv.Op {
			case "is_empty":
				cdExprs = append(cdExprs, fmt.Sprintf("(is_null(%s))", cdv.Left.Field))
			case "is_not_empty":
				cdExprs = append(cdExprs, fmt.Sprintf("(is_not_null(%s))", cdv.Left.Field))
			case "in":
				m, err := cast.ToStringMapE(cdv.Right)
				if err != nil {
					return "", err
				}
				members, err := cast.ToSliceE(m["members"])
				if err != nil {
					return "", err
				}
				bytes, err := json.Marshal(members)
				if err != nil {
					return "", err
				}
				expr := fmt.Sprintf("(%s in %v)", cdv.Left.Field, string(bytes))
				expr = strings.ReplaceAll(strings.ReplaceAll(expr, "[", "("), "]", ")")
				cdExprs = append(cdExprs, expr)
			case "not_in":
				m, err := cast.ToStringMapE(cdv.Right)
				if err != nil {
					return "", err
				}
				members, err := cast.ToSliceE(m["members"])
				if err != nil {
					return "", err
				}
				bytes, err := json.Marshal(members)
				if err != nil {
					return "", err
				}
				expr := fmt.Sprintf("(!(%s in %v))", cdv.Left.Field, string(bytes))
				expr = strings.ReplaceAll(strings.ReplaceAll(expr, "[", "("), "]", ")")
				cdExprs = append(cdExprs, expr)
			case "starts_with":
				val, err := cast.ToStringE(cdv.Right)
				if err != nil {
					return "", err
				}
				cdExprs = append(cdExprs, fmt.Sprintf("(starts_with(%s, %s))", cdv.Left.Field, val))
			case "ends_with":
				val, err := cast.ToStringE(cdv.Right)
				if err != nil {
					return "", err
				}
				cdExprs = append(cdExprs, fmt.Sprintf("(ends_with(%s, %s))", cdv.Left.Field, val))
			default:
				cdExprs = append(cdExprs, fmt.Sprintf("(%s %s %#v)", cdv.Left.Field, ConditionOperators[cdv.Op], cdv.Right))
			}
		case dto.RuleConditions:
			res, err := ParseCondition(cdv)
			if err != nil {
				return "", err
			}
			cdExprs = append(cdExprs, res)
		default:
			return "", fmt.Errorf("unknown condition type: %T", cdv)
		}
	}

	return strings.Join(cdExprs, fmt.Sprintf(" %s ", LogicalOperators[conditions.Conjunction])), nil
}

func executeLogic(expression string, data map[string]interface{}) (bool, error) {
	expr, err := govaluate.NewEvaluableExpressionWithFunctions(expression, RuleEngineFuncs)
	if err != nil {
		return false, err
	}
	result, err := expr.Evaluate(data)
	if err != nil {
		return false, err
	}

	if r, ok := result.(bool); ok {
		return r, nil
	} else {
		return false, fmt.Errorf("result is not a boolean")
	}
}

func ExecuteProjectRuleSet(defaultOutput []dto.OutputVar, ruleSet []dto.RuleSet, data map[string]interface{}) (map[string]interface{}, string, error) {
	optVars := make(map[string]interface{})
	optVarTypes := make(map[string]string)
	for _, outputVar := range defaultOutput {
		val, err := ValidateRuleSetOutput(outputVar)
		if err != nil {
			return nil, "", err
		}
		optVars[outputVar.VarName] = val
		optVarTypes[outputVar.VarName] = outputVar.VarType
	}

	executeLogs := make([]string, 0)
	for _, rule := range ruleSet {
		rule := rule
		expr, err := ParseCondition(rule.RuleConditions)
		if err != nil {
			return nil, "", err
		}

		result, err := executeLogic(expr, data)
		if err != nil {
			return nil, "", err
		}

		if result {
			for _, outputVar := range rule.RuleOutput {
				outputVar := outputVar
				outputVar.VarType = optVarTypes[outputVar.VarName]

				val, err := ValidateRuleSetOutput(outputVar)
				if err != nil {
					return nil, "", err
				}

				optVars[outputVar.VarName] = val
				data[outputVar.VarName] = val
			}

			bytes, err := json.Marshal(optVars)
			if err != nil {
				return nil, "", err
			}
			executeLogs = append(executeLogs, fmt.Sprintf("Rule [%s] executed", rule.RuleName))
			executeLogs = append(executeLogs, string(bytes))
		}
	}

	var executeLog string
	if len(executeLogs) > 0 {
		executeLog = strings.Join(executeLogs, "\n")
	} else {
		executeLog = "No rules executed"
	}

	return optVars, executeLog, nil
}
