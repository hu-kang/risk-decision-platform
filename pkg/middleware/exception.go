package middleware

import (
	"fmt"
	"risk-decision-platform/pkg/response"

	"github.com/gin-gonic/gin"
)

func CustomeRecover(c *gin.Context) {
	defer func() {
		if err := recover(); err != nil {
			response.Failed(c, fmt.Sprintf("%v", err))
			c.Abort()
		}
	}()
	c.Next()
}

func HandleError(c *gin.Context) {
	c.Next()

	// handle error
	for _, err := range c.Errors {
		if err != nil {
			response.Failed(c, err.Error())
			c.Abort()
			return
		}
	}
}
