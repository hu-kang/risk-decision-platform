package database

type (
	MysqlConfig struct {
		Host     string
		Port     int
		User     string
		Password string
		Database string
	}

	PGConfig struct {
		Host     string
		Port     int
		User     string
		Password string
		Database string
	}

	MongoConfig struct {
		Host string
		Port int
	}
)
