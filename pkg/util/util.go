package util

import (
	"fmt"

	"github.com/spf13/cast"
)

func GetSlicePagination(page int, perPage int, src interface{}) (interface{}, error) {
	slice, err := cast.ToSliceE(src)
	if err != nil {
		return nil, err
	}

	startIndex := (page - 1) * perPage
	endIndex := startIndex + perPage

	if endIndex > len(slice) {
		endIndex = len(slice)
	}

	result := slice[startIndex:endIndex]
	if result == nil {
		return make([]interface{}, 0), nil
	} else {
		return result, nil
	}
}

func GetDefaultValue(valueType string, defaultValue interface{}) (interface{}, error) {
	if defaultValue == "" || defaultValue == nil {
		return nil, nil
	}

	switch valueType {
	case "INT":
		return cast.ToIntE(defaultValue)
	case "FLOAT":
		return cast.ToFloat64E(defaultValue)
	case "STRING":
		return cast.ToStringE(defaultValue)
	case "BOOLEAN":
		return cast.ToBoolE(defaultValue)
	default:
		return nil, fmt.Errorf("unsupported value type: %s", valueType)
	}
}
